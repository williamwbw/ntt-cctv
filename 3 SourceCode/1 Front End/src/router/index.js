import Vue from 'vue'
import Router from 'vue-router'

// Containers
const TheContainer = () => import('@/containers/TheContainer')

// Views
const Dashboard = () => import('@/views/dashboard/Dashboard')

const Login = () => import('@/views/pages/Login')
const ScreenView = () => import('@/views/pages/ScreenView')

// Cameras
const NewCameraScreen = () => import('@/views/dashboard/NewCameraScreen')
const NewCamera = () => import('@/views/dashboard/NewCamera')

// Settings
//const Setting = () => import('@/views/dashboard/SettingPage')
const LocationSetup = () => import('@/views/dashboard/Location')

const test = () => import('@/views/pages/test');


Vue.use(Router)

export default new Router({
  mode: 'hash', // https://router.vuejs.org/api/#mode
  linkActiveClass: 'active',
  scrollBehavior: () => ({ y: 0 }),
  routes: configRoutes()
})

function configRoutes () {
  return [
    {
      path: '/',
      redirect: '/Login',
      name: 'Home',
      component: Login,
      children: [
        {
          path: 'login',
          name: 'Login',
          component: Login
        },
      ]
    },
    {
      path: '/',
      redirect: '/ScreenView',
      name: 'ScreenView',
      component: ScreenView,
      children: [
        {
          path: 'ScreenView',
          name: 'ScreenView',
          component: ScreenView
        },
      ]
    },
    {
      path: '/',
      redirect: '/test',
      name: 'test',
      component: test,
      children: [
        {
          path: 'test',
          name: 'test',
          component: test
        },
      ]
    },
    {
      path: '/',
      redirect: '/Home/Dashboard',
      name: 'Home',
      component: TheContainer,
      children: [
        {
          path: 'Dashboard',
          name: 'Dashboard',
          component: Dashboard
        },
        {
          path: 'CameraScreen',
          name: 'Camera',
          component: NewCameraScreen
        },
        {
          path: 'NewCamera',
          name: 'New Camera',
          component: NewCamera
        },
        {
          path: 'Location',
          name: 'Location Setting',
          component: LocationSetup
        }
      ]
    }
  ]
}

