export default [
  {
    _name: 'CSidebarNav',
    _children: [
      {
        _name: 'CSidebarNavItem',
        name: 'Dashboard',
        to: '/dashboard',
        icon: 'cil-speedometer'
      },
      {
        _name: 'CSidebarNavDropdown',
        name: 'Setting',
        to: '/Setting',
        icon: 'cilAppsSettings',
        items: [
          {
              name: 'Camera Setting',
              to: '/NewCamera'
          },
          {
            name: 'Camera Screen',
            to: '/CameraScreen'
          },
          {
            name: 'Location',
            to: '/Location'
          }
        ]
      }
    ]
  }
]