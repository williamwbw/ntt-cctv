module.exports = {
  lintOnSave: false,
  runtimeCompiler: true,
  configureWebpack: {
    //Necessary to run npm link https://webpack.js.org/configuration/resolve/#resolve-symlinks
    resolve: {
       symlinks: false
    }
  },
  transpileDependencies: [
    '@coreui/utils'
  ],
  chainWebpack: config => {
    config.module
      .rule('swf')
      .test(/\.swf$/)
      .use('url-loader')
        .loader('url-loader')
        // eslint-disable-next-line no-unused-vars
        .tap(options => {
          return {
            limit: 10000
          }
        })
  },
  devServer: {
    proxy: 'https://10.115.12.79:9080/config/',
  }
}
