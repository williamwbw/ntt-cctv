﻿using Castle.Components.DictionaryAdapter.Xml;
using NTT_API.Realtimedata;
using NTT_API.SessionContext;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Xml;
using MessageHeader = NTT_API.SessionContext.MessageHeader;

namespace NTT_API.Controllers
{
    public class POCController : ApiController
    {
        // GET: api/POC
        public IEnumerable<string> Get()
        {
            ServicePointManager.ServerCertificateValidationCallback = new System.Net.Security.RemoteCertificateValidationCallback(AcceptAllCertifications);
            WebRequest request = WebRequest.Create("https://192.168.0.102/WebClient/");
            request.Credentials = new NetworkCredential("User01", "1234");
            request.Method = "POST";
            request.ContentType = "application/json";
            request.ContentLength = 0;

            Realtimedata.RealTimeDataSoapClient soap = new Realtimedata.RealTimeDataSoapClient(endpointConfigurationName: "RealTimeDataSoap");
            soap.ClientCredentials.UserName.UserName = "User01";
            soap.ClientCredentials.UserName.Password = "1234";

            Realtimedata.UpdateValuesRequest Request = new Realtimedata.UpdateValuesRequest();
            UpdateValue update = new UpdateValue();
            update.Name = "TEMP";
            update.Timestamp = DateTime.Now;
            update.Quality = 0;
            update.Value = "test123";
            Realtimedata.BrowseResponse response = new Realtimedata.BrowseResponse();
            return new string[] { "value1", "value2" };
        }

        // GET: api/POC/5
        public List<VariableCollection> Get(int id)
        {
            ServicePointManager.ServerCertificateValidationCallback = new System.Net.Security.RemoteCertificateValidationCallback(AcceptAllCertifications);
            WebRequest request = WebRequest.Create("https://192.168.0.102/WebClient/");
            request.Credentials = new NetworkCredential("User01", "1234");
            request.Method = "POST";
            request.ContentType = "application/json";
            request.ContentLength = 0;

            Realtimedata.RealTimeDataSoapClient soap = new Realtimedata.RealTimeDataSoapClient(endpointConfigurationName: "RealTimeDataSoap");
            soap.ClientCredentials.UserName.UserName = "User01";
            soap.ClientCredentials.UserName.Password = "1234";

            string SessionID = "";
            MessageHeader message = new MessageHeader();
            message.MessageIssuer = "User01";
            message.MessageTimeStamp = DateTime.Now.ToString();
            message.Signature = "User01";

            Realtimedata.MessageHeader msg = new Realtimedata.MessageHeader();
            msg.MessageIssuer = "User01";
            msg.MessageTimeStamp = DateTime.Now.ToString();
            msg.Signature = "User01";
            SessionContext.SVLanguage sV = SessionContext.SVLanguage.ContextDefault;
            SessionContext.SessionContext svSession = new SessionContext.SessionContext();
            SessionContext.Result result = svSession.OpenSession("User01", "1234", sV, out SessionID);
            VariableCollectionIterator vr = new VariableCollectionIterator();
            
            Realtimedata.BrowseRequest Request = new Realtimedata.BrowseRequest();
            Request.SessionId = SessionID;
            Request.MessageHeader = msg;
            Request.iterator = vr;
            Realtimedata.BrowseResponse response = new Realtimedata.BrowseResponse();

            var test = soap.BrowseAsync(Request).Result;
            


            return test.replyData.variableCollections.ToList();
        }

        // POST: api/POC
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/POC/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/POC/5
        public void Delete(int id)
        {
        }

        public bool AcceptAllCertifications(object sender, System.Security.Cryptography.X509Certificates.X509Certificate certification, System.Security.Cryptography.X509Certificates.X509Chain chain, System.Net.Security.SslPolicyErrors sslPolicyErrors)
        {
            return true;
        }

        public string InvokeService()
        {
            //Calling CreateSOAPWebRequest method    
            HttpWebRequest request = CreateSOAPWebRequest();
            
            XmlDocument SOAPReqBody = new XmlDocument();
            //SOAP Body Request    
            SOAPReqBody.LoadXml(@"<?xml version=""1.0"" encoding=""utf-8""?>
                                    <soap:Envelope xmlns:xsi = ""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd = ""http://www.w3.org/2001/XMLSchema"" xmlns:soap = ""http://schemas.xmlsoap.org/soap/envelope/"" >
                                      <soap:Header >
                                        <MessageHeader xmlns=""http://WebServicesToolkit.net/SV/SessionContext/v1.0/"" >
                                          <MessageTimeStamp>string</MessageTimeStamp>
                                          <MessageIssuer>string</MessageIssuer>
                                          <Signature>string</Signature>
                                        </MessageHeader>
                                      </soap:Header>
                                      <soap:Body>
                                        <OpenSession xmlns=""http://WebServicesToolkit.net/SV/SessionContext/v1.0/"" >
                                          <UserName>User01</UserName>
                                          <Password>1234</Password>
                                          <projectLanguage>BaseLanguage</projectLanguage>
                                        </OpenSession>
                                      </soap:Body>
                                    </soap:Envelope> ");

            XmlNodeList datanodes = SOAPReqBody.GetElementsByTagName("MessageHeader");

            using (Stream stream = request.GetRequestStream())
                    {
                        SOAPReqBody.Save(stream);
                    }
                    //Geting response from request    
                    using (WebResponse Serviceres = request.GetResponse())
                    {
                        using (StreamReader rd = new StreamReader(Serviceres.GetResponseStream()))
                        {
                            //reading stream    
                            var ServiceResult = rd.ReadToEnd();
                            //writting stream result on console    
                            Console.WriteLine(ServiceResult);
                            Console.ReadLine();
                    return ServiceResult;
                        }
                    }
            }

        public HttpWebRequest CreateSOAPWebRequest()
        {
            //Making Web Request    
            HttpWebRequest Req = (HttpWebRequest)WebRequest.Create(@"https://192.168.0.102/SessionContext/SessionContext.asmx?WSDL");
            //SOAPAction    
            Req.Headers.Add(@"SOAPAction:http://WebServicesToolkit.net/SV/SessionContext/v1.0/OpenSession");
            //Content_type    
            Req.ContentType = "text/xml;charset=\"utf-8\"";
            Req.Accept = "text/xml";
            //HTTP method    
            Req.Method = "POST";
            //return HttpWebRequest    
            return Req;
        }
    }
}
